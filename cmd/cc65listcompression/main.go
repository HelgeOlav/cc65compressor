package main

import (
	"bitbucket.org/HelgeOlav/cc65compressor/compressor"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	compressor.ShowCompressors()
	if len(os.Args) == 1 {
		fmt.Printf("Usage: %v filename - show compression for given file\n", os.Args[0])
		os.Exit(1)
	}
	printCompressions(os.Args[1])
}

func printCompressions(name string) {
	// read file
	b, err := ioutil.ReadFile(name)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// print some info
	fmt.Printf("Input file: %v, size %v bytes.\n", name, len(b))
	if len(b) > 65535 {
		fmt.Println("Warning! This file is too big for the 6500 CPU memory")
	}
	// try the compressors
	for _, v := range compressor.Compressors {
		c := v(nil)
		c.Compress(b)
		fmt.Printf("%v: compressed size %v, overhead %v, total %v bytes\n", c.Name(), len(c.Result()), c.Overhead(), len(c.Result())+c.Overhead())
	}
	// compressor.RleFindMagicByte([]byte{1, 2, 3, 1, 0, 1, 0, 9, 2})
}
