package main

import (
	"bitbucket.org/HelgeOlav/cc65compressor/compressor"
	_ "bitbucket.org/HelgeOlav/cc65compressor/compressor"
	"fmt"
	"os"
)

// test function to make a binary file with 1000 bytes with the value 01
func makeWhite() {
	of, _ := os.Create("whitecolorscreen-c64.bin")
	defer of.Close()
	b := []byte{1}
	for i := 0; i < 1000; i++ {
		of.Write(b)
	}
}

func makeScreenData() {
	of, _ := os.Create("screentext-c64.bin")
	defer of.Close()
	b := []byte{8, 5, 17, 16, 5, 32}
	of.Write(b)
	//b = []byte{ 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1, 1, 1, 1 ,1 ,1, 1}
	for i := 1; i < 10; i++ {
		b = []byte{}
		for y := 0; y < 10; y++ {
			b = append(b, byte(i))
		}
		of.Write(b)
	}
	of.Write([]byte{20})

}

func main() {
	// makeWhite()
	//makeScreenData()
	parseCmdLine()
	pf := ProcessFile{
		Infile:         *InFile,
		Outfile:        *OutFile,
		Cs:             *CodeSegment,
		Ds:             *DataSegment,
		Startpos:       *StartLocation,
		Module:         *ModuleName,
		UseConstructor: *IsConstructor,
	}
	if c, ok := compressor.Compressors[*CompressorType]; ok {
		pf.Compressor = c(nil)
	} else {
		fmt.Printf("Unknown compressor %v.\n", *CompressorType)
		compressor.ShowCompressors()
		os.Exit(1)
	}
	pf.Init()
	defer pf.Close()
	pf.Compress()
	pf.WriteHeader()
	pf.WriteCode()
	pf.WriteData()
}
