package compressor

// This compressor adds everything literally without compressing the data.

func init() {
	Compressors["null-compressor"] = NewNullCompressor
}

func NewNullCompressor(interface{}) Compressor {
	np := new(NullCompressor)
	return np
}

type NullCompressor struct {
	result []byte
}

//TODO: possibly one-off on copying (one byte too much?)
func (n *NullCompressor) GetCode() string {
	myCode := `;main
.importzp ptr1, ptr2, ptr3 ; ptr1=source, prt2=dest, ptr3=counter
 lda #<writeto
 sta ptr2
 lda #>writeto
 sta ptr2+1
 lda #<compresseddata
 sta ptr1
 lda #>compresseddata
 sta ptr1+1
 lda #<compressedlen
 sta ptr3
 lda #>compressedlen
 sta ptr3+1
 ldx #0
 ldy #$ff 
loop:
 lda (ptr1, x)
 sta (ptr2, x)
; increment dest (ptr2)
 inc ptr2
 bne @L1
 inc ptr2+1
@L1:
; increment source (ptr1)
 inc ptr1
 bne @L2
 inc ptr1+1
@L2:
; decrement counter (ptr3)
 dec ptr3
 cpy ptr3
 bne loop
 dec ptr3+1
 cpy ptr3+1
 beq done
 jmp loop
done:
 rts
`
	return myCode
}

func (n *NullCompressor) Compress(bytes []byte) error {
	n.result = bytes
	return nil
}

func (n *NullCompressor) Overhead() int {
	return 60
}

func (n *NullCompressor) Name() string {
	return "null-compressor"
}

func (n *NullCompressor) Result() []byte {
	return n.result
}
