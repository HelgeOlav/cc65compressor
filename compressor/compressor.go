package compressor

import "fmt"

// This interface has to be implemented by the different compressors
type Compressor interface {
	Compress([]byte) error // compress an array of bytes
	Overhead() int         // return this decompressor own code estimated size
	Name() string          // name of this compressor
	Result() []byte        // get the compressed result
	// Return the code between .proc and .endproc
	// The following variables are known: writeto, compresseddata, compressedlen
	GetCode() string
}

// Compressors can register themselves here
var Compressors = map[string]func(interface{}) Compressor{}

// Print all configured compressors to screen
func ShowCompressors() {
	fmt.Println("The following compressors are configured:")
	for k, _ := range Compressors {
		fmt.Printf(" %v\n", k)
	}
}
