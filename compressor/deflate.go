package compressor

import (
	bytes2 "bytes"
	"compress/flate"
)

// This file implements the deflate compression

func init() {
	Compressors["deflate"] = NewDeflateCompressor
}

func NewDeflateCompressor(interface{}) Compressor {
	return new(DeflateCompressor)
}

type DeflateCompressor struct {
	result []byte
}

func (d *DeflateCompressor) GetCode() string {
	myCode := `;main
.import _inflatemem, pushax
 lda #<writeto
 ldx #>writeto
 jsr pushax
 lda #<compresseddata
 ldx #>compresseddata
 jmp _inflatemem
`
	return myCode
}

func (d *DeflateCompressor) Compress(bytes []byte) error {
	d.result = []byte{}
	// make a writer
	var buf bytes2.Buffer
	flateWriter, err := flate.NewWriter(&buf, flate.BestCompression)
	if err != nil {
		return err
	}

	// compress
	_, err = flateWriter.Write(bytes)
	if err != nil {
		return err
	}
	flateWriter.Close()
	d.result = buf.Bytes()
	return nil
}

func (d *DeflateCompressor) Overhead() int {
	return 1320
}

func (d *DeflateCompressor) Name() string {
	return "deflate"
}

func (d *DeflateCompressor) Result() []byte {
	return d.result
}
