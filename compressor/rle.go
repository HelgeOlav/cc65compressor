package compressor

import (
	bytes2 "bytes"
)

// This compressor adds everything literally without compressing the data.

func init() {
	Compressors["rle"] = NewRleCompressor
}

func NewRleCompressor(interface{}) Compressor {
	np := new(RleCompressor)
	return np
}

type RleCompressor struct {
	result      []byte
	MagicNumber byte
}

func (n *RleCompressor) Overhead() int {
	return 82
}

func (n *RleCompressor) Name() string {
	return "rle"
}

func (n *RleCompressor) Result() []byte {
	return n.result
}

/*
The RLE data header is always:
1) byte 127 to indicate start of data (simple verification)
2) byte the magic number used for decompression
RLE data stream is (always a byte) is either
1) a byte that will be copied directly over
2) a magic number indicating that what follows is either
 a) EOD - the decompression is done
 b) one byte - number of repetitions (1..255) followed by one byte that is to be copied # times

If the MagicNumber is the byte to be copied it has to be encoded as (magic, 1, magic) to get out one time to the destination.
*/

const RleStartOfData = 127

// const RleMagicNumber = 200 // magic number that indicates a control code
const RleEOD = 0 // byte following the magic number, indicating end of data (we are done)

func RleFindMagicByte(bytes []byte) byte {
	repetitions := make(map[byte]int)
	// loop through and see occurrences
	for _, v := range bytes {
		repetitions[v]++
	}
	// fmt.Println(repetitions)
	var goodCandidate = 0
	var goodCandidateFreq = len(bytes) + 1
	if len(repetitions) == 256 { // all number has been used
		// find a number that has been least used
		for k, v := range repetitions {
			if goodCandidateFreq > v {
				goodCandidate = int(k)
				goodCandidateFreq = v
			}
		}
	} else { // find an unused number
		var i byte
		for i = 0; i <= 255; i++ {
			if repetitions[i] == 0 {
				goodCandidate = int(i)
				break
			}
		}
	} // else if
	return byte(goodCandidate)
}

// find the number of similar bytes ahead in buffer starting from pos 0. Max 255.
// should never return 0 unless input is empty
func RleFindRepetitions(bytes []byte) byte {
	// sanity check
	bytesLen := len(bytes)
	// fmt.Println(bytesLen)
	if bytesLen == 0 {
		return 0
	}
	if bytesLen == 1 {
		return 1
	}
	// start counting
	var count byte = 1
	myByte := bytes[0]
	for {
		// see if we have found max repetitions
		if count == 255 {
			break
		}
		// check if there is a next byte
		// fmt.Println(myByte, bytesLen, int(count))
		//TODO: fix code below, something is wrong
		if bytesLen <= int(count) {
			break
		}
		// check current byte
		if bytes[count] != myByte {
			break
		}
		count++
	}
	return count
}

func (n *RleCompressor) Compress(bytes []byte) error {
	if n.MagicNumber != 0 {
		n.MagicNumber = RleFindMagicByte(bytes)
	}
	var buf bytes2.Buffer
	// write header
	buf.Write([]byte{RleStartOfData, n.MagicNumber})
	// look through
	pos := 0
	bufLen := len(bytes)
	for {
		// see if we are done
		if pos >= bufLen {
			break
		}
		// check next byte
		reps := RleFindRepetitions(bytes[pos:])
		myByte := bytes[pos]
		// is it repeated less than 4 times and is not magic number then just write out the value directly
		if reps <= 3 && myByte != n.MagicNumber {
			for x := 0; x < int(reps); x++ {
				buf.Write([]byte{myByte})
			}
			pos = pos + int(reps)
			continue
		}
		// is it repeated?
		if reps > 1 {
			buf.Write([]byte{n.MagicNumber, reps, myByte})
			pos = pos + int(reps)
			continue
		}
		// is it the magic number?
		if myByte == n.MagicNumber {
			buf.Write([]byte{myByte, 1, myByte})
			pos++
			continue
		}
		// send the byte through
		buf.Write([]byte{myByte})
		pos++
	}
	// write EOD
	buf.Write([]byte{n.MagicNumber, RleEOD})
	// finish up
	n.result = buf.Bytes()
	// debug
	// fmt.Printf("Magic number: %v %d\n", n.MagicNumber, n.result)
	return nil
}

func (d *RleCompressor) GetCode() string {
	myCode := `.importzp ptr1, ptr2, ptr3 ; ptr2=source ptr1=dest, ptr3=magic
rle_header = $7f
 lda #<writeto
 sta ptr1
 lda #>writeto
 sta ptr1+1
 lda #<compresseddata
 sta ptr2
 lda #>compresseddata
 sta ptr2+1
; verify header
 ldy #0
 lda (ptr2), y
 cmp #rle_header
 bne done   ; bail out - not my data
 jsr incsrcby1
 lda (ptr2), y
 sta ptr3  ; this is our magic number
 jsr incsrcby1
; now let's read data
loop:
 ldx #1 ; num of repeats
 lda (ptr2), y
 cmp ptr3
 beq @L3
; adjust input, +1
 jsr incsrcby1
 jmp writeout
@L3:
; get repeats
 jsr incsrcby1
 lda (ptr2), y
 beq done ; check if we are done
 tax
; get byte
 jsr incsrcby1
 lda (ptr2), y
; time to write data
; A = content
; X = repeats
; Y = 0
writeout:
 sta (ptr1), y
 inc ptr1
 bne @L4
 inc ptr1+1
@L4:
 dex
 bne writeout
 jmp loop
; helper that increments source address by 1
incsrcby1:
 inc ptr2
 bne done
 inc ptr2+1
done:
 rts
`
	return myCode
}
