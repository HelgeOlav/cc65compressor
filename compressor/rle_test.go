package compressor

import (
	"bytes"
	"testing"
)

func TestRleFindMagicByte1(t *testing.T) {
	input := []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	mb := RleFindMagicByte(input)
	if mb != 10 {
		t.Errorf("TestRleFindMagicByte1 expected 10, got %v", mb)
	}
}

func TestRleFindMagicByte2(t *testing.T) {
	input := []byte{}
	for x := 0; x < 256; x++ {
		input = append(input, byte(x))
	}
	for x := 1; x < 256; x++ {
		input = append(input, byte(x))
	}
	mb := RleFindMagicByte(input)
	if mb != 0 {
		t.Errorf("TestRleFindMagicByte1 expected 0, got %v", mb)
	}
}

func TestRleFindRepetitions1(t *testing.T) {
	// one byte, another in the list
	b := []byte{1, 2}
	res := RleFindRepetitions(b)
	if res != 1 {
		t.Errorf("TestRleFindRepetitions1 expected 1, got %v", res)
	}
	// one byte alone
	b = []byte{1}
	res = RleFindRepetitions(b)
	if res != 1 {
		t.Errorf("TestRleFindRepetitions1 expected 1, got %v", res)
	}
	// empty list
	b = []byte{}
	res = RleFindRepetitions(b)
	if res != 0 {
		t.Errorf("TestRleFindRepetitions1 expected 0, got %v", res)
	}
	// two equal bytes, another byte at end of list
	b = []byte{3, 3, 2}
	res = RleFindRepetitions(b)
	if res != 2 {
		t.Errorf("TestRleFindRepetitions1 expected 2, got %v", res)
	}
	// two equal bytes at end of list
	b = []byte{3, 3}
	res = RleFindRepetitions(b)
	if res != 2 {
		t.Errorf("TestRleFindRepetitions1 expected 2, got %v", res)
	}
	// max out with 255 bytes
	b = []byte{255}
	for x := 0; x < 300; x++ {
		b = append(b, 255)
	}
	res = RleFindRepetitions(b)
	if res != 255 {
		t.Errorf("TestRleFindRepetitions1 expected 255, got %v", res)
	}
	// max out with 254 bytes
	res = RleFindRepetitions(b[0:254])
	if res != 254 {
		t.Errorf("TestRleFindRepetitions1 expected 254, got %v", res)
	}

}

func TestRleCompressor_Compress1(t *testing.T) {
	input := []byte{1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6}
	c := new(RleCompressor)
	c.MagicNumber = 10
	err := c.Compress(input)
	if err != nil {
		t.Errorf("TestRleCompressor_Compress1 %v", err)
	}
	expected := []byte{RleStartOfData, c.MagicNumber, 1, 2, 2, 3, 3, 3, c.MagicNumber, 4, 4, c.MagicNumber, 5, 5, c.MagicNumber, 6, 6, c.MagicNumber, RleEOD}
	if bytes.Equal(expected, c.result) == false {
		t.Errorf("TestRleCompressor_Compress1\n%v expected\n%v got", expected, c.result)
	}
}
