package main

import (
	"bitbucket.org/HelgeOlav/cc65compressor/compressor"
	"flag"
	"fmt"
	"os"
)

var InFile *string         // input filename
var OutFile *string        // output filename
var DataSegment *string    // segment to place data into
var CodeSegment *string    // segment to place code into
var StartLocation *int     // where to expand the code to
var FileSize int64         // size of file
var ModuleName *string     // name of module
var IsConstructor *bool    // set if we want to run automatically
var CompressorType *string // compressor to use

func parseCmdLine() {
	InFile = flag.String("in", "", "Input filename")
	OutFile = flag.String("out", "", "Output filename (.s)")
	DataSegment = flag.String("ds", "RODATA", "Linker segment for the data")
	CodeSegment = flag.String("cs", "CODE", "Linker segment for the code")
	StartLocation = flag.Int("deflate-to", 49152, "Where to deflate the binary to (decimal)")
	ModuleName = flag.String("module", "initmemory", "name of exported symbol for module")
	IsConstructor = flag.Bool("constructor", false, "add .constructor to generated source code")
	CompressorType = flag.String("compressor", "rle", "what compressor to use")
	NeedHelp := flag.Bool("help", false, "show this screen")
	flag.Parse()
	// should we print help screen
	if *NeedHelp {
		flag.Usage()
		compressor.ShowCompressors()
		os.Exit(1)
	}
	// check input file
	if len(*InFile) == 0 {
		fmt.Println("Missing input file, exiting")
		os.Exit(1)
	}
	// check input file length
	fi, err := os.Stat(*InFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	FileSize = fi.Size()
	if FileSize > 65535 {
		fmt.Println("Input file is over 64k, quitting")
		os.Exit(1)
	}
	// check boundary
	if int(FileSize)+*StartLocation > 65535 {
		fmt.Println("Binary file crossing 64k boundary")
		os.Exit(1)
	}
	// check output filename
	if len(*OutFile) == 0 {
		*OutFile = *InFile + ".deflate.s"
	}
	// check for placement region
	if *StartLocation < 0 {
		fmt.Println("Invalid --deflate-to")
		os.Exit(1)
	}
}
