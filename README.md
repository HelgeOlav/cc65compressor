# cc65compressor

This tool compressses binary files into source kode files that can be added to your CC65 project. The file is compressed
using one of the supported algorithms. The deflate decompression algorithm is the one included in the standard
library. The others are included in the created source code.

## Usage

```
cc65compressor -in myfile.bin -out myfile.s --deflate-to 49152 --module _myfile
```

At this point you have an assembly file that can be included into your project. You can add several files by changing
the module name.

If running with C code you have the option of decompress automatically on startup by specifing ```--constructor=true```.
If you do this you can also place code and data in the ONCE segment that will be recycled for other use when main() is called.
This is not supported with deflate.

You can also choose to call the code directly from your code.

To use it from assembler write code like this:
```
.import _myfile
.code
 jsr _myfile
```

If you want to include it from your C code you can use code like this:
```c
void myfile();

void mycode() {
 myfile();
}
```
## Compressors

There are three compressors in the distribution.

* rle
* deflate (using CC65 run-time)
* null-compressor (just copying data)

The default is rle, you can specify what compressor to use by ```--compressor=null-compressor```.

## Limitations - deflate

It is possible to put code and data into other segments if you want to do that. As inflatemem() uses BSS it is not possible
to use ONCE for the data, nor add this as a constructor that loads the data automatically. (The BSS is not initialized
when the constructors run.)
